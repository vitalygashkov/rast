require('module-alias/register');
const http = require('http'),
      RastAPI = require('@RastAPI'),
      RastServer = http.Server(RastAPI),
      RastPort = 3000,
      LOCAL = 'localhost';

RastServer.listen(RastPort, LOCAL, () => console.log(`RastAPI running on ${RastPort}`));